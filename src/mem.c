#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"


void debug_block(struct block_header* b, const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region ( void const * addr, size_t query ) {
  bool is_extended = true;

  block_size r_size = (block_size) {region_actual_size(size_from_capacity((block_capacity) {query}).bytes)};
  void* r_addr = map_pages(addr, r_size.bytes, MAP_FIXED_NOREPLACE);

  if (r_addr == MAP_FAILED) {
    //next try with indefinite addr
    is_extended = false;
    r_addr = map_pages(addr, r_size.bytes, 0);
    if (r_addr == MAP_FAILED) {
      return REGION_INVALID;
    }
  }

  struct region new_region = {
     .addr = r_addr,
     .size = r_size.bytes,
     .extends = is_extended
  };

  block_init(r_addr, r_size, NULL);
  return new_region;
}

static void* block_after( struct block_header const* block );


void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}


#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {

    if (!block_splittable(block, query)) return false;

    block_size size =  (block_size) {block->capacity.bytes - query};
    block->capacity = (block_capacity) {query};

    void *addr = block_after(block);
    block_init(addr, size, block->next);
    block->next = addr;

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (block == NULL || block->next == NULL || !mergeable(block, block->next)) return false;

  block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
  block->next = block->next->next;

  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last ( struct block_header* restrict block, size_t sz )    {

  while(true){
    if (block->is_free) {

      while(true) {
        if (!try_merge_with_next(block)) break;
      }
      if (block_is_big_enough(sz, block)){
        return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
      }
    }

    if (!block->next) break;
    block = block->next;
  }

  return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};

}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
//находим, делим и занимаем
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {

 struct block_search_result found_res = find_good_or_last(block, query);
  if (found_res.type != BSR_FOUND_GOOD_BLOCK) return found_res;

  split_if_too_big(found_res.block, query);
  found_res.block->is_free = false;

  return found_res;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
 
  if (!last) return NULL;
  struct region r = alloc_region( block_after(last), query );
  struct region* new_region = &r;

  if (region_is_invalid(new_region)) return NULL;

  last->next = new_region->addr;
  if(!try_merge_with_next(last)){
    return last->next;
  };

  return last;  
}


/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {

  query = size_max(query, BLOCK_MIN_CAPACITY);
  struct block_header *grown;

  struct block_search_result found_res = try_memalloc_existing( query, heap_start );
  switch(found_res.type) {

    case BSR_FOUND_GOOD_BLOCK:
      return found_res.block;

    case BSR_REACHED_END_NOT_FOUND:
      grown = grow_heap( found_res.block, query );
      if (!grown) return NULL;

      found_res = try_memalloc_existing(query, grown);
      if (found_res.type == BSR_FOUND_GOOD_BLOCK) return found_res.block;
      return NULL;

    case BSR_CORRUPTED:
      return NULL;
      break;
  }

  return NULL;

}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {

  if (!mem) return;
  struct block_header* h = block_get_header(mem);
  h->is_free = true;
  while(h) {
    try_merge_with_next(h);
    h = h->next;
  }
}
