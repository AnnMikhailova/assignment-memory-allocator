#define _DEFAULT_SOURCE

#include "test.h"

#include <inttypes.h>
#include <stdio.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define TEST_HEAP_SIZE 10000
#define TEST_BLOCK_SIZE 100

static void* test_heap;

enum test_malloc_status {
    TEST_SUCCESS,
    TEST_ERROR,
    BLOCK_NULL_VALUE_ERROR,
    MEM_ALLOCATION_ERROR,
    BLOCK_NOT_FREE_ERROR
};

char *test_malloc_status_desc[] = {
        [TEST_SUCCESS] = "Test finished. STATUS: OK.",
        [TEST_ERROR] = "Test failed.",
        [BLOCK_NULL_VALUE_ERROR] = "Test failed. ERROR: Block is null.",
        [MEM_ALLOCATION_ERROR] = "Test failed. ERROR: Memory allocation was invalid.",
        [BLOCK_NOT_FREE_ERROR] = "Test failed. ERROR: Tried to delete a block. Block state is not free after operation."
};

static void debug_heap_with_comment(const char* comment);

static void init_heap(){
    test_heap = heap_init(TEST_HEAP_SIZE);
    if (!test_heap) {
        err ("ERROR: Heap initialization failed.");
    }
    debug_heap_with_comment("---Heap added:\n");
}

static void free_heap(){
    struct block_header* block = (struct block_header*) test_heap;
    while (block) {
	if (!block->is_free) _free(block->contents);
	block = block->next;
    }
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

/*
    Обычное успешное выделение памяти.
*/
enum test_malloc_status test1() {
    debug("------------------\nTest 1 >>>\n");
    debug_heap_with_comment("Before block added:\n");
    void* block = _malloc(TEST_BLOCK_SIZE);
    if (!block) return BLOCK_NULL_VALUE_ERROR;
    debug_heap_with_comment("Block added:\n");

    struct block_header* block_header = block_get_header(block);
    if (block_header->capacity.bytes != TEST_BLOCK_SIZE || block_header->is_free) {
        return MEM_ALLOCATION_ERROR;
    }
    return TEST_SUCCESS;
}

/*
    Освобождение одного блока из нескольких выделенных.
*/
enum test_malloc_status test2() {
    debug("------------------\nTest 2 >>>\n");
    void* block1 = _malloc(TEST_BLOCK_SIZE);
    void* block2 = _malloc(TEST_BLOCK_SIZE);
    if (!block1 || !block2) return BLOCK_NULL_VALUE_ERROR;
    
    debug_heap_with_comment("Two blocks added:\n");
    _free(block1);
    debug_heap_with_comment("After removing one block:\n");

    if (!block_get_header(block1)->is_free) return BLOCK_NOT_FREE_ERROR;
    return TEST_SUCCESS;
}

/*
    Освобождение двух блоков из нескольких выделенных.
*/
enum test_malloc_status test3() {
    debug("------------------\nTest 3 >>>\n");
    void* block1 = _malloc(TEST_BLOCK_SIZE);
    void* block2 = _malloc(TEST_BLOCK_SIZE);
    void* block3 = _malloc(TEST_BLOCK_SIZE);
    if (!block1 || !block2 || !block3) return BLOCK_NULL_VALUE_ERROR;

    debug_heap_with_comment("Three blocks added:\n");
    _free(block1);
    _free(block2);
    debug_heap_with_comment("After removing two block:\n");

    if (!block_get_header(block1)->is_free || !block_get_header(block2)->is_free) return BLOCK_NOT_FREE_ERROR;
    return TEST_SUCCESS;
}


static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

struct block_header* get_last_block() {
    for (struct block_header* last = (struct block_header*) test_heap; ; last = last->next) {
        if (last->next == NULL) {
            return last;
        }
    }
}

/*
    Память закончилась, новый регион памяти расширяет старый.
*/
enum test_malloc_status test4() {
    debug("------------------\nTest 4 >>>\n");
    debug_heap_with_comment("Heap:\n");
    struct block_header* last_block = get_last_block();

    size_t heap_size_bigger = TEST_HEAP_SIZE * 2;
    void* big_block = _malloc(heap_size_bigger);
    if (!big_block) return BLOCK_NULL_VALUE_ERROR;
    debug_heap_with_comment("Extended heap:\n");

    struct block_header* block_header = block_get_header(big_block);
    if (block_header->capacity.bytes != heap_size_bigger || block_header->is_free || !blocks_continuous(last_block, block_header)) return MEM_ALLOCATION_ERROR;
    return TEST_SUCCESS;
}


static void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*
    Память закончилась, старый регион памяти не расширить 
    из-за другого выделенного диапазона адресов, 
    новый регион выделяется в другом месте.
*/
enum test_malloc_status test5() {
    debug("------------------\nTest 5 >>>\n");
    debug_heap_with_comment("Heap:\n");

    struct block_header* last_block = get_last_block();
    void* addr = map_pages(block_after(last_block), TEST_HEAP_SIZE, MAP_FIXED);
    if (addr == MAP_FAILED) return TEST_ERROR;

    size_t block_bigger_than_heap_size = TEST_HEAP_SIZE*5;
    struct block_header* new_block = _malloc(block_bigger_than_heap_size);
    debug_heap_with_comment("After mapping in other place:\n");

    if (blocks_continuous(last_block, new_block)) return MEM_ALLOCATION_ERROR;
    return TEST_SUCCESS;    
}


static void write_status( enum test_malloc_status status) {
    if (!status){
        debug(test_malloc_status_desc[status]);
        debug("\n\n");
    } else {
        err(test_malloc_status_desc[status]);
    }
}

static void debug_heap_with_comment(const char* comment) {
    if (comment) {
        debug(comment);
    }
    debug_heap(stderr, test_heap);
}

void test_malloc_execute() {
    init_heap();

    write_status(test1());
    free_heap();

    write_status(test2());
    free_heap();

    write_status(test3());
    free_heap();

    write_status(test4());
    free_heap();

    write_status(test5());
    free_heap();

    debug("All tests passed.\n");
}

